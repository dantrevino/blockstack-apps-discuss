# blockstack-apps-discuss

First, read this:
https://blockstack.com/tokenpaper.pdf

This will answer a lot of questions about directions for app developers and users.

* What is the typical Business model for Apps developed over Blockstack

> A Blockstack app is just an app, but with better data protections for individual users.  Mining user's data for ads is technically possible, but then why build a Blockstack app if you're going to turn your users into a product?  In terms of "typical" business models, any usage based fee structure could work.  We need more information on Stacks token protocol, but soon the protocol will enable users to reward/incentivize developers directly.
> 
> Those are the more mundane/mainstream options, I think there are many opportunities to innovate on business models here.

* How dApps creators generate revenue

> Make great apps.  Not trying to be glib, but this doesnt change.  The *scale* of revenue might not be the same as a great App/Play store app **today**, but that will change. :slight_smile: 

* Can advertising be done on dApps for example on Afari

> Sure, why not?  There's a huge opportunity to give users control over what they share, in exchange for a "free" service, but technically you could still mine away at your users ~~trust~~ usage.  There is nothing in the current platform to prevent that.

* How users can get reward for Data they share publicly or to other Analytics app

> Stacks token protocol?  I would say look at contribute.blockstack.org for a similar model.

* Role of Blockstack in building Enterprise applications

> Blockstack provides a great identity platform to build on.  Imagine being able to secure your org structure via blockstack ids and subdomains.

* Any role for 3rd party advertising agency

> You can still collect analytics.  You should be honest with your users, but nothing in the platform prevents you from running bad ad networks in your app.  See the comment above about potential protocol implementations that may dis-incentivize this though.

* How to prevent dApps (stealthy) from wrong usage (ex. hackers, terrorist agencies) and any model built to detect &amp; prevent such usage. If so who will incur the cost (dApp or Blockstack)

> Prevent, no.  Discourage, yes.  See Stack token protocol.

* Why should we invest in dApps created using Blockstack

> I would say, don't invest in Dapps (I like to emphasize the "D"ecentralized part, not the app part) using Blockstack.  I would say invest in great applications.  Blockstack just make doing some of the hard data protections and identity management much easier.

* How will Government agency investigate the user data (in case of any suspicion) and will there be any decrypt key that will be provided to Federal agencies.

> They wont.  Local laws vary on how a user could/would have to turn over a private key, but the private key is the only way in to a user's data.  There is nothing to prevent an app from saving a copy of the users data, however see Stacks token protocol for dis-incentivation mechanisms.

* Business model of Blockstack when compared to other Blockchain technologies (Eth, hyperledger).